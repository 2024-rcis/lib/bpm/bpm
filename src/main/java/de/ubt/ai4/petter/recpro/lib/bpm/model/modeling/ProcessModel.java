package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsProcessModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessModel extends BpmElement {
    private String xml;
    private List<Activity> activities = new ArrayList<>();
    private List<Role> roles = new ArrayList<>();

    public static ProcessModel fromBpmsProcessModel(BpmsProcessModel processModel) {
        ProcessModel result = new ProcessModel();
        result.setId(processModel.getId());
        result.setElementType(ElementType.PROCESS_MODEL);
        result.setXml(processModel.getXml());
        result.setActivities(Activity.fromBpmsActivities(processModel.getActivities()));
        result.setRoles(Role.fromBpmsRoles(processModel.getRoles()));
        return result;
    }
}
