package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BpmElement {
    private List<Role> childElements = new ArrayList<>();

    public static Role fromBpmsRole(BpmsRole role) {
        Role result = new Role();
        result.setId(role.getId());
        result.setName(role.getName());
        result.setElementType(ElementType.ROLE);
        result.setChildElements(Role.fromBpmsRoles(role.getChildElements()));
        return result;
    }

    public static List<Role> fromBpmsRoles(List<BpmsRole> roles) {
        return roles.stream().map(Role::fromBpmsRole).toList();
    }
}
