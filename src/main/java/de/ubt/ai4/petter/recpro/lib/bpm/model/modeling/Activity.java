package de.ubt.ai4.petter.recpro.lib.bpm.model.modeling;

import de.ubt.ai4.petter.recpro.lib.bpms.modeling.model.BpmsActivity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Activity extends BpmElement {
    private List<Role> roles = new ArrayList<>();

    public static Activity fromBpmsActivity(BpmsActivity activity) {
        Activity result = new Activity();
        result.setId(activity.getId());
        result.setName(activity.getName());
        result.setElementType(ElementType.PROCESS_ACTIVITY);
        result.setRoles(Role.fromBpmsRoles(activity.getRoles()));
        return result;
    }

    public static List<Activity> fromBpmsActivities(List<BpmsActivity> activities) {
        return activities.stream().map(Activity::fromBpmsActivity).toList();
    }
}
